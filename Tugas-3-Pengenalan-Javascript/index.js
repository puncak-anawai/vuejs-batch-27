//soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var ketiga = pertama.substr(0,5);
var keempat = pertama.substr(12,7);
var kelima = kedua.substr(0,8);
var keenam = kedua.substr(8,10).toUpperCase();

console.log(ketiga + keempat + kelima + keenam);

//soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var angkaPertama = Number(kataPertama);
var angkaKedua = Number(kataKedua);
var angkaKetiga = Number(kataKetiga);
var angkaKeempat = Number(kataKeempat);

console.log(angkaKedua * angkaKetiga + angkaKeempat + angkaPertama);

//soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4,14);
var kataKetiga = kalimat.substring(15,18);
var kataKeempat = kalimat.substring(19,24); 
var kataKelima = kalimat.substring(25,31);

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);