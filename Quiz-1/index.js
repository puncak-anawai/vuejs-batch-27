// SOal 1
function next_date(tanggal, bulan) {
    
    var tanggal_besok;

        if (tanggal == 28 && bulan == 2) {
            tanggal_besok = 1;
        } else if (tanggal == 30) {
            if (bulan == 1 || bulan == 3 || bulan == 5 || bulan == 7 || bulan == 8 || bulan == 10 || bulan == 12) {
                tanggal_besok = 31;
            } else {
                tanggal_besok = 1;
            }
        } else if (tanggal == 31) {
            tanggal_besok = 1;
        } else {
            tanggal_besok = tanggal + 1;
        }
    
    var bulan_besok;

        if (tanggal == 28 && bulan == 2) {
            bulan_besok = bulan + 1;
        } else if (tanggal == 30) {
            if (bulan == 4 || bulan == 6 || bulan == 9 ||bulan == 11) {
            bulan_besok = bulan + 1;
            } else if (bulan == 1 || bulan == 3 || bulan == 5 || bulan == 7 || bulan == 8 || bulan == 10 || bulan == 12) {
            bulan_besok = bulan; }
        } else if (tanggal == 31) {
            if (bulan == 1 || bulan == 3 || bulan == 5 || bulan == 7 || bulan == 8 || bulan == 10) {
            bulan_besok = bulan + 1; }
            else if (bulan == 12) {
            bulan_besok = 1; }
        } else {
            bulan_besok = bulan;
        }

    var bulan_huruf_besok = "";

    switch(bulan_besok) {
        case 1:
            bulan_huruf_besok ="Januari";
            break;
        case 2:
            bulan_huruf_besok ="Februari";
            break;
        case 3:
            bulan_huruf_besok ="Maret";
            break;
        case 4:
            bulan_huruf_besok ="April";
            break;
        case 5:
            bulan_huruf_besok ="Mei";
            break;
        case 6:
            bulan_huruf_besok ="Juni";
            break;
        case 7:
            bulan_huruf_besok ="Juli";
            break;
        case 8:
            bulan_huruf_besok ="Agustus";
            break;
        case 9:
            bulan_huruf_besok ="September";
            break;
        case 10:
            bulan_huruf_besok ="Oktober";
            break;
        case 11:
            bulan_huruf_besok ="November";
            break;
        case 12:
            bulan_huruf_besok ="Desember";
            break;
        default:
            bulan_huruf_besok ="Bulan Kosong"
      }

    var tahun_besok;

    if (tanggal == 31 && bulan == 12) {
        tahun_besok = tahun + 1;
    } else {
        tahun_besok = tahun;
    }

    console.log(tanggal_besok + " "  + bulan_huruf_besok + " " + tahun_besok);

}

var tanggal = 29;
var bulan = 3;
var tahun = 2020;

next_date(tanggal, bulan, tahun);

//Soal 2
var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = "Saya Iqbal"

jumlah_kata(kalimat_1) // 6
jumlah_kata(kalimat_2) // 2

function jumlah_kata(string){
    var trim_kalimat = string.trim();
    var split_kalimat = trim_kalimat.split(" ");
    var banyak_kata = split_kalimat.length;

    console.log(banyak_kata);
}

