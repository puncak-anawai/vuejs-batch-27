//Soal 1

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var urut = daftarHewan.sort();

for(var i=0; i<daftarHewan.length; i++) {
    console.log(urut[i]);
}

//Soal 2

/* 
    Tulis kode function di sini
*/

function introduce(object) {
    var name = object.name;
    var age = object.age;
    var address = object.address;
    var hobby = object.hobby;
    var kalimat = `Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}`;
    return kalimat;
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }

var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"

//Soal 3

function cek_huruf_vokal(string) {
    return ['a', 'i', 'u', 'e', 'o'].indexOf(string) !== -1;
}

function hitung_huruf_vokal(string) {
    var huruf_kecil = string.toLowerCase();
    var kata_array = huruf_kecil.split("");
    var jumlah_huruf_vokal = 0;

    for (i=0; i<kata_array.length; i++) {
        if (cek_huruf_vokal(kata_array[i]) === true) {
            jumlah_huruf_vokal += 1;
        };
    } 

    return jumlah_huruf_vokal;
}
var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2

//Soal 4
function hitung(integer) {
    var angka = integer;
    var n = -2;
    var output = 0;

    for(var i=0; i<=angka; i++) {
        if (i == angka) {
            return n;
        } else {
            n+= 2;
        }
    }
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8