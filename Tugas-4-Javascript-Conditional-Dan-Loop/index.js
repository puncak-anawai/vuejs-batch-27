//soal 1
var nilai = 50;
var index;

if (nilai >= 85){
    index = "A";
} else if (nilai >= 75 && nilai < 85){
    index = "B";
} else if (nilai >= 65 && nilai < 75){
    index = "C";
} else if (nilai >= 55 && nilai < 65){
    index = "D";
} else {
    index = "E";
}

console.log("Jawaban soal nomor 1");
console.log("Nilai kamu adalah : " + nilai);
console.log("Dengan predikat : " + index);

//soal 2
var tanggal = 10;
var bulan = 02;
var tahun = 1997;
var bulanHuruf = "";

switch(bulan) {
    case 1:   { bulanHuruf = "Januari"; break; }
    case 2:   { bulanHuruf = "Februari"; break; }
    case 3:   { bulanHuruf = "Maret"; break; }
    case 4:   { bulanHuruf = "April"; break; }
    case 5:   { bulanHuruf = "Mei"; break; }
    case 6:   { bulanHuruf = "Juni"; break; }
    case 7:   { bulanHuruf = "Juli"; break; }
    case 8:   { bulanHuruf = "Agustus"; break; }
    case 9:   { bulanHuruf = "September"; break; }
    case 10:   { bulanHuruf = "Oktober"; break; }
    case 11:   { bulanHuruf = "November"; break; }
    case 12:   { bulanHuruf = "Desember"; break; }
    default:  { console.log('Anda Belum Mengisi Bulan'); }};

console.log("Jawaban soal nomor 2");
console.log(tanggal + " " + bulanHuruf + " " + tahun);

//soal 3

var jumlahPagar = 10;
var pagar = "";

console.log("Jawaban soal nomor 3");

for(i=1; i<=jumlahPagar; i++) {
    pagar += "#";
    console.log(pagar);
}

//soal 4
var m = 21;
var n = 1;
var o = 2;
var p = 3;
var samaDengan = "";

console.log("Jawaban soal nomor 4");

for(var i=1; i<=m; i++){
    if(i == n){
        console.log(i + " - I love Programming");
        n += 3;
    } else if (i == o){
        console.log(i + " - I love Javascript");
        o += 3;
    } else if (i == p){
        console.log(i + " - I love VueJS");
        p += 3;
        samaDengan += "===";
        console.log(samaDengan);
    }
}

// for(i=1; i<=m; i++) {
//     if(i%2 == 1 && i%3!= 0){
//         console.log(i + " - I love Programming");
//     } else if (i%2 == 0 && i%3 != 0){
//         console.log(i + " - I love Javascript");
//     } else if (i%3 == 0){
//         console.log(i + " - I love VueJS");
//         samaDengan += "===";
//         console.log(samaDengan);
//     }
// }
